@extends('layout')

@section('content')
        <div class="row">
    @foreach ($products as $product)
        <div class="col-md-4 mb-3">
            <div class="card h-100">
                <div class="card-body">
                    <h5 class="card-title">
                        {{ $product->name }}
                    </h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ $product->sku }}</h6>
                    <p class="card-text">{{ $product->description }}</p>
                    <a href="/products/{{ $product->id }}" class="card-link">View Product</a>
                </div>
            </div>
        </div>
    @endforeach
        </div>
@endsection
