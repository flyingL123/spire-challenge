# Spire Challenge #

This is a sample Laravel project for the Spire Challenge. Use this guide to get started and complete the Challenge Tasks.

### Setup ###
* You will need a Bitbucket account. If you don't have one, [sign up here](https://bitbucket.org/account/signup/).
* [Fork this repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) into your own Bitbucket account, then clone the fork onto your local machine. If you don't have git installed on your computer, [install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
* PHP uses a tool called Composer to manage project dependencies. [Install Composer](https://getcomposer.org/doc/00-intro.md). If you're installing on a Linux/Unix/MacOS system, be sure to run `mv composer.phar /usr/local/bin/composer` after installation to ensure it is installed globally on your system. This will allow you to run composer commands more easily.
* In the project root directory, run the command `composer install` to install the project dependencies. Then run the command `php artisan serve`. In your browser, go to [http://127.0.0.1:8000](http://127.0.0.1:8000) to view the sample store.
* You may find the [Laravel Documentation](https://laravel.com/docs/5.8/) helpful.
* These are some key files to look through while trying to solve the Challenge Tasks along with related Laravel documentation:
    * `routes/web.php` ([routing](https://laravel.com/docs/5.8/routing))
    * `app/Http/Controllers/ProductsController.php` ([controllers](https://laravel.com/docs/5.8/controllers))
    * `resources/views/products` directory ([views](https://laravel.com/docs/5.8/views), [blade templates](https://laravel.com/docs/5.8/blade))

### Background Information ###

This application represents an ecommerce store. The store contains 15 preconfigured products. 11 of those products are marked as a featured product. A featured product contains the attribute `is_featured = true`. Additionally, 7 of the products are marked as a best seller. A best seller contains the attribute `is_best_seller = true`.

The "All Products" page lists all of the products in the store. If you click the "View Product" link for any product, you will be brought to that product's individual product page.

You will also notice a "Featured Products" page, which is currently blank. Populating this page is part of the challenge.

### Challenge Tasks ###

* Populate the "Featured Products" page. The page should be identical to the "All Products" page, except it should only contain products that are featured.
* On the page for an individual product, if the product is a best seller, add a [Bootstrap Badge](https://getbootstrap.com/docs/4.0/components/badge/) to indicate its best seller status.

### Need Help? ###

If you need any help at all, do not hesitate to ask. I am very aware that you may not have any experience yet with Laravel or even PHP, so it may be a challenge just to get this application running. That's totally fine. Part of the challenge is asking the right questions, so don't hesitate to ask for help!

### When You're Done ###

After you have completed the challenge tasks, push your updates to your Bitbuck repository. Then send me the link to your repo so I can view the updates you have made.
