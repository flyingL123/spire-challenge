<?php

namespace App\Console\Commands;

use Faker\Generator as Faker;
use Illuminate\Console\Command;

class GenerateBigQuerySales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bigquery-sales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Output JSON file to populate BigQuery sales table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Faker $faker)
    {
        $products = collect();

        $sales = collect();

        foreach (range(1, 20) as $i) {
            $products->push([
                'sku' => $faker->unique()->numerify('SPIRE-###'),
                'price' => $faker->randomFloat(2, 10, 100),
            ]);
        }

        foreach (range(1, 1000) as $i) {
            $numProducts = rand(1, 3);

            $sale = [
                'order_id' => $i,
                'customer_name' => $faker->name,
                'state' => $faker->state,
            ];

            for ($i=0; $i<$numProducts; $i++) {
                $product = $products->random(); 
            
                $sale['product'][] = [
                    'sku' => $product['sku'],
                    'quantity' => rand(1, 4),
                    'price' => $product['price'],
                ];
            }

            $sales->push($sale);
        }

        $sales->each(function ($sale) {
            echo json_encode($sale)."\r\n";
        });
    }
}
